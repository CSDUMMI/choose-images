FROM python:3.9

COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt

EXPOSE 8888
CMD cd src && gunicorn  -b 0.0.0.0:8888 --log-file - main:app
