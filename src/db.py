from peewee import *
import peeweedbevolve
import os

db = SqliteDatabase("/db/images.db")

class BaseModel(Model):
    class Meta:
        database = db


class Photo(BaseModel):
    photo_id = IntegerField(primary_key = True)

    def score(self):
        return sum([v.vote for v in self.votes])

class Vote(BaseModel):
    photo = ForeignKeyField(Photo, backref="votes")
    vote = IntegerField()

if os.environ.get("CREATE_TABLES", default = False) == "true":
    db.connect()
    db.create_tables([Photo, Vote])
    db.close()
