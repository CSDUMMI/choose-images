from flask import Flask, jsonify, request, render_template, session
import db
import os
import datetime

app = Flask(__name__,
            template_folder = "../templates",
            static_url_path = "/static",
            static_folder = "../static"
        )

SECRET_KEY = os.environ["SECRET_KEY"]
app.config.from_object(__name__)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/vote/<int:photo_id>/<int:vote>", methods=["POST"])
def vote(photo_id, vote):
    vote -= 1
    session["votes"] = session.get("votes", default = -1)

    if session["votes"] > photo_id:
        return jsonify({  "voted": False, "end": False})

    session["votes"] = photo_id
    end = photo_id >= len(os.listdir("../static/images"))

    (photo, _) = db.Photo.get_or_create(photo_id = photo_id)

    db.Vote.create(vote = vote, photo = photo)
    return jsonify({ "voted": True, "end": end})

@app.route("/result", methods = ["POST", "GET"])
def result():
    sliceAt = int(request.values["sliceAt"])

    photos = sorted([photo for photo in db.Photo.select()], key = lambda p: p.score())[:sliceAt]

    result_id = datetime.datetime.now()
    os.mkdir(f"../results/{result_id}")
    for photo in photos:
        os.symlink(f"../static/images/{photo.photo_id}.JPG", f"../results/{result_id}/{photo.photo_id}.JPG")

    return jsonify({
        "votes": db.Vote.select().count(),
        "results": [{
            "photo_id" : photo.photo_id,
            "score" : photo.score(),
            } for photo in photos]
    })
