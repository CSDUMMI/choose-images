module Vote exposing (main)
import Browser
import Http
import Html exposing (Html)
import Element as E
import Element.Input as Input
import Element.Font as Font
import Element.Background as Background
import Json.Decode as D
import Octicons exposing (defaultOptions)

main =
  Browser.document
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }

-- MODEL
type Vote
  = FOR
  | AGAINST
  | NEUTRAL

voteToInt : Vote -> Int
voteToInt vote =
  case vote of
    FOR -> 2
    AGAINST -> 0
    NEUTRAL -> 1

type alias Model
  = { photo_id : Int
    , vote : Maybe Vote
    , end : Bool
    }
init : () -> (Model, Cmd Msg)
init _ = ({ photo_id = 0
          , vote = Nothing
          , end = False
          }, Cmd.none)

-- UPDATE
type Msg
  = VoteMsg Vote
  | Submitted (Result Http.Error SubmitResult)

type alias SubmitResult
  = { end : Bool
    , voted : Bool
    }

update : Msg -> Model -> (Model, Cmd Msg)
update msg model
  = case msg of
      VoteMsg vote -> ({ model | vote = Just vote}, submit model.photo_id vote)
      Submitted result ->
        case result of
          Ok submitResult -> ({ model | photo_id = model.photo_id + 1, end = submitResult.end }, Cmd.none)
          Err _ -> (model, Cmd.none)

submit : Int -> Vote -> Cmd Msg
submit photo_id vote
  = Http.post
      { url = "/vote/" ++ String.fromInt photo_id ++ "/" ++ String.fromInt (voteToInt vote)
      , body = Http.emptyBody
      , expect = Http.expectJson Submitted submitDecoder
      }

submitDecoder : D.Decoder SubmitResult
submitDecoder =
  D.map2 SubmitResult
    (D.field "end" D.bool)
    (D.field "voted" D.bool)

-- VIEW
toIcon : (Octicons.Options -> Html Msg) -> E.Element Msg
toIcon icon =
  defaultOptions
  |> Octicons.color "white"
  |> Octicons.size 24
  |> icon
  |> E.html

forIcon : E.Element Msg
forIcon = toIcon Octicons.thumbsup

againstIcon : E.Element Msg
againstIcon = toIcon Octicons.thumbsdown

neutralIcon : E.Element Msg
neutralIcon = toIcon Octicons.law

black = E.rgb255 0 0 0
white = E.rgb255 255 255 255

view : Model -> Browser.Document Msg
view model
  = { title = "Choose Images"
    , body = [
      E.layout [Background.color black, E.height E.fill] <|
        case model.end of
          True -> viewEnd
          False ->
            E.column
              [E.centerX, E.spaceEvenly]
              [ E.image
                [ E.width E.fill
                , E.height <| E.maximum 540 <| E.fillPortion 4
                ] { src = "/static/images/" ++ String.fromInt model.photo_id ++ ".JPG"
                           , description = "Photo #" ++ String.fromInt model.photo_id
                           }
              , E.row [E.height <| E.fillPortion 1, E.width E.fill] <|
                List.map
                (\(v, l) -> Input.button [E.width E.fill]
                  { onPress = Just <| VoteMsg v
                  , label = l
                  }) [(AGAINST, againstIcon), (NEUTRAL, neutralIcon), (FOR, forIcon)]
              ]
      ]
    }


viewEnd : E.Element Msg
viewEnd
  = E.el [E.centerX, E.centerY, Font.size 24, Font.color white] <| E.text "Thank you by CSDUMMI"

subscriptions : Model -> Sub Msg
subscriptions _ = Sub.none
