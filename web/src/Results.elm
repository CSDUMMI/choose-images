module Results exposing (main)
import Browser
import Http
import Html
import Element as E
import Element.Input as Input
import Element.Font as Font
import Element.Border as Border
import Json.Decode as D


main =
  Browser.document
    { init = init
    , view = view
    , update = update
    , subscriptions = \_ -> Sub.none
    }

-- MODEL
type alias VoteResult
  = { score : Int
    , photo_id : Int
    }

src : VoteResult -> String
src voteResult = "/static/images/" ++ String.fromInt voteResult.photo_id ++ ".JPG"

type alias Model
  = { votes : Int
    , results : List VoteResult
    , selected : Maybe Int
    }

voteResultDecoder : D.Decoder VoteResult
voteResultDecoder =
  D.map2 VoteResult
    (D.field "score" D.int)
    (D.field "photo_id" D.int)

modelDecoder : D.Decoder Model
modelDecoder =
  D.map2 (\votes results -> { votes = votes, results = results, selected = Nothing })
    (D.field "votes" D.int)
    (D.field "results" <| D.list voteResultDecoder)


init : () -> (Model, Cmd Msg)
init _ = ({ votes = 0
          , results = []
          , selected = Nothing
          }, fetchResults)

-- UPDATE
type Msg
  = ResultsReceived (Result Http.Error Model)
  | SelectImage Int
  | ReturnToAll


update : Msg -> Model -> (Model, Cmd Msg)
update msg model
  = case msg of
      ResultsReceived result ->
        case result of
          Ok model_ -> (model_, Cmd.none)
          Err _ -> (model, Cmd.none)
      SelectImage index ->
        if index < (List.length model.results) && index >= 0
          then ({ model | selected = Just index }, Cmd.none)
          else (model, Cmd.none)
      ReturnToAll ->
        ({ model | selected = Nothing }, Cmd.none)

fetchResults : Cmd Msg
fetchResults
  = Http.get
      { url = "/results"
      , expect = Http.expectJson ResultsReceived modelDecoder
      }

-- VIEW
view : Model -> Browser.Document Msg
view model
  = { title = "Choose Images | Results"
    , body = [
      E.layout [] <|
        case model.selected of
          Just index ->
            viewImage index model.results
          Nothing ->
            viewAllImages model.results
    ]
    }

viewImage : Int -> List VoteResult -> E.Element Msg
viewImage index results
  = let
      result = case List.head <| List.drop index results of
                Just h -> h
                Nothing -> { photo_id = -1, score = 0 }
      msg = "#" ++ String.fromInt result.photo_id ++ " with score " ++ String.fromInt result.score
    in E.column
      [ E.centerX
      , E.spaceEvenly
      , E.padding 50
      , E.above <| Input.button []
          { onPress = Just ReturnToAll
          , label = E.el [Font.size 16] <| E.text "x"
          }
      ]
      [ image
          { src = src result
          , description = msg
          }
      , E.el [Font.size 16] << E.text <| msg ++ " at position #" ++ String.fromInt index ++ " in ranking"
      , E.row [E.height E.fill, E.width E.fill]
          [ Input.button []
              { onPress = Just (SelectImage <| index - 1)
              , label = E.el [Font.size 16] <| E.text "<"
              }
          , Input.button []
              { onPress = Just (SelectImage <| index + 1)
              , label = E.el [Font.size 16] <| E.text ">"
              }
          ]
      ]

viewAllImages : List VoteResult -> E.Element Msg
viewAllImages results
  = let toImage index result =
          Input.button []
            { onPress = Just (SelectImage index)
            , label = image
                { src = src result
                , description = String.fromInt result.photo_id
                }
            }

    in E.wrappedRow [E.padding 50]
        << List.indexedMap toImage <| List.sortBy (\r -> r.score) results

image : { src : String, description : String } -> E.Element Msg
image opts =
  E.image [E.width <| E.minimum 200 <| E.maximum 320 <| E.fill, E.height <| E.fillPortion 4] opts
